import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { PreviewComponent } from './preview/preview.component';
import { PublishComponent } from './publish/publish.component';

const routes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'preview' , component: PreviewComponent},
  {path: 'publish' , component: PublishComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [RegisterComponent, PreviewComponent, PublishComponent];
