import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  data;
  constructor() { }

  ngOnInit() {
  this.data = JSON.parse(localStorage.getItem('details'));
  console.log(this.data);
  }
}
